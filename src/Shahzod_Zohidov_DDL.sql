CREATE DATABASE mountain_club;

CREATE SCHEMA mountain_club;

CREATE TABLE mountain_club.Climbers (
    ClimberID serial PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    Address TEXT,
    PhoneNumber VARCHAR(15),
    Email VARCHAR(100) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.Mountains (
    MountainID serial PRIMARY KEY,
    MountainName VARCHAR(100) NOT NULL,
    Height INTEGER NOT NULL,
    Country VARCHAR(50) NOT NULL,
    Region VARCHAR(50) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.Climbs (
    ClimbID serial PRIMARY KEY,
    ClimberID INT REFERENCES mountain_club.Climbers(ClimberID) NOT NULL,
    MountainID INT REFERENCES mountain_club.Mountains(MountainID) NOT NULL,
    StartDate DATE CHECK (StartDate > '2000-01-01'),
    EndDate DATE,
    UNIQUE (ClimberID, MountainID),
    NOT NULL (EndDate),
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.ClimbingPartners (
    PartnerID serial PRIMARY KEY,
    ClimberID INT REFERENCES mountain_club.Climbers(ClimberID) NOT NULL,
    ClimbID INT REFERENCES mountain_club.Climbs(ClimbID) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.Countries (
    CountryID serial PRIMARY KEY,
    CountryName VARCHAR(50) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.Regions (
    RegionID serial PRIMARY KEY,
    RegionName VARCHAR(50) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.ClimateZones (
    ClimateID serial PRIMARY KEY,
    ClimateName VARCHAR(50) NOT NULL,
    CountryID INT REFERENCES mountain_club.Countries(CountryID) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.ClimbingGear (
    GearID serial PRIMARY KEY,
    GearName VARCHAR(100),
    GearDescription VARCHAR(200),
    ClimberID INT REFERENCES mountain_club.Climbers(ClimberID) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.ClimbedMountains (
    ClimberID INT REFERENCES mountain_club.Climbers(ClimberID) NOT NULL,
    MountainID INT REFERENCES mountain_club.Mountains(MountainID) NOT NULL,
    record_ts DATE DEFAULT current_date
);

CREATE TABLE mountain_club.Languages (
    LanguageID serial PRIMARY KEY,
    LanguageName VARCHAR(50) NOT NULL,
    record_ts DATE DEFAULT current_date
);

ALTER TABLE mountain_club.Climbers
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.Mountains
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.Climbs
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.ClimbingPartners
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.Countries
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.Regions
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.ClimateZones
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.ClimbingGear
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.ClimbedMountains
ADD record_ts DATE DEFAULT current_date;

ALTER TABLE mountain_club.Languages
ADD record_ts DATE DEFAULT current_date;


